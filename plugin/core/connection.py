import json
import time
from queue import Queue
from threading import Thread

from ...dep import websocket
from .logger import info
from .typing import Callable


class WebsocketManager:
    def __init__(
        self,
        endpoint: str,
        callback_processor: Callable,
        callback_connection: Callable,
    ) -> None:
        self._endpoint = endpoint
        self.callback_processor = callback_processor
        self.callback_connection = callback_connection
        self._send_queue = Queue()
        self._recv_queue = Queue()
        self.ws = None  # type: websocket.WebSocketApp
        self._wst = None  # type: Thread
        self._send_queue_running = False
        self._recv_queue_running = False
        self._create_join_response = False

    def start(self) -> bool:
        info(f'[WebsocketManager] connecting to {self._endpoint}')
        self.ws = websocket.WebSocketApp(
            self._endpoint,
            on_message=lambda ws, msg: self._on_recv_payload(ws, msg),
            on_error=lambda ws, msg: self._on_error(ws, msg),
            on_close=lambda ws: self._on_close(ws),
        )
        self._wst = Thread(target=self.ws.run_forever, daemon=True)
        self._wst.start()

        self._send_thread = Thread(target=self._process_send_queue, daemon=True)
        self._send_queue_running = True
        self._send_thread.start()

        self._recv_thread = Thread(target=self._process_recv_queue, daemon=True)
        self._recv_queue_running = True
        self._recv_thread.start()

        if not _connection_established(self.ws.sock, 10):
            self.stop()
            return False

        timeout = 5
        while not self._create_join_response and timeout != 0:
            timeout -= 1
            time.sleep(0.5)

        if not self._create_join_response:
            self.stop()
            return False

        return True

    def stop(self) -> None:
        self._send_queue_running = False
        self._recv_queue_running = False

        with self._send_queue.mutex:
            self._send_queue.queue.clear()

        with self._recv_queue.mutex:
            self._recv_queue.queue.clear()

        if self.ws and self.ws.sock and self.ws.sock.connected:
            self.ws.close()

    def toggle_create_join_response(self, enabled):
        self._create_join_response = enabled

    def send_payload(self, payload: dict, exit_payload: bool = False):
        if exit_payload and self.ws.sock and self.ws.sock.connected:
            self.ws.send(json.dumps(payload))
        else:
            self._send_queue.put(payload)

    def _process_send_queue(self):
        while self._send_queue_running:
            payload = self._send_queue.get()
            try:
                raw_payload = json.dumps(payload)
                self.ws.send(raw_payload)
                self._send_queue.task_done()
            except Exception as ex:
                print(ex)

    def _process_recv_queue(self):
        while self._recv_queue_running:
            payload = self._recv_queue.get()
            self.callback_processor(payload)
            self._recv_queue.task_done()

    def _on_recv_payload(self, _: websocket.WebSocketApp, raw_payload: str) -> None:
        try:
            payload = json.loads(raw_payload)
        except Exception as e:
            info(f'[WebsocketManager] {e}')
            return

        if payload.get('type', None) == 'ping':
            return
        self._recv_queue.put(payload)

    def _on_error(self, _: websocket.WebSocketApp, error):
        self.callback_connection()
        info(f'_on_error:\t {error}')

    def _on_close(self, _: websocket.WebSocketApp):
        info('[WebsocketManager] Closed Connection')
        self.callback_connection()


def _connection_established(socket: websocket.WebSocket, timeout: int) -> bool:
    timer = 0
    while timer < timeout:
        timer += 1
        if socket and socket.connected:
            return True
        time.sleep(0.15)
    return False
