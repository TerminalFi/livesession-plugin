import sublime
import sublime_plugin

from weakref import ref
from weakref import WeakSet
from weakref import WeakValueDictionary

from ...util.region_helper import encrypt_regions
from ...util.syntax import get_syntax_info
from ...util.util import new_iv
from ...util.view_helper import get_center_view_line_offset

from .logger import debug
from .manager import session_manager
from .manager import SessionManager
from .payloads import (
    gen_region_info,
    new_closed_payload,
    new_incremental_payload,
    new_open_payload,
    new_selection_payload,
)
from .typing import Iterable, List, Optional


class LiveSessionTextChangeListener(sublime_plugin.TextChangeListener):

    ids_to_listeners = (
        WeakValueDictionary()
    )  # type: WeakValueDictionary[int, LiveSessionTextChangeListener]

    @classmethod
    def is_applicable(cls, buffer: sublime.Buffer) -> bool:
        # FIXME: Cannot reliably use primary_view().file_name()
        return buffer.primary_view().element() is None  # type: ignore

    def __init__(self) -> None:
        super().__init__()
        self.view_listeners = WeakSet()

    def attach(self, buffer: sublime.Buffer) -> None:
        super().attach(buffer)
        self.ids_to_listeners[self.buffer.buffer_id] = self

    def detach(self) -> None:
        self.ids_to_listeners.pop(self.buffer.buffer_id, None)
        super().detach()

    def on_text_changed_async(self, changes: Iterable[sublime.TextChange]) -> None:
        for listener in list(self.view_listeners):
            listener.on_text_changed_async(changes)

    def __repr__(self) -> str:
        return "LiveSessionTextChangeListener({})".format(self.buffer.buffer_id)


class LiveSessionEventListener(sublime_plugin.EventListener):
    ACTIVE_CLIENTS = 'livesession.active_clients'

    def on_new(self, view):
        '''
        Called when the file is finished loading.

        LiveSession: TYPE_FILE_SWITCHED | TYPE_FILE_OPENED
        '''
        if not session_manager.enabled_for_view(view):
            return

        view_ref = session_manager.add_view_to_session(view)
        if not view_ref:
            return

        self._update_client_status(view)

        viewport = get_center_view_line_offset(view)
        iv, regions = encrypt_regions(
            [
                {
                    'a': 0,
                    'b': view.size(),
                    'content': view.substr(sublime.Region(0, view.size())),
                }
            ],
            session_manager.session.key,
            new_iv(),
            session_manager.session.encrypted,
        )

        syntax_info = get_syntax_info(view)
        payload = new_open_payload(
            uid=session_manager.session_id(),
            passphrase=session_manager.session_passphrase(),
            encrypted=session_manager.session.encrypted,
            host=session_manager.session_host(),
            sender=session_manager.me(),
            regions=[
                gen_region_info(
                    file_id=view.settings().get('livesession.view.uid'),
                    file_name=view_ref['relative_name'],
                    iv=iv,
                    syntax=syntax_info,
                    viewport={'a': viewport[0], 'b': viewport[1]},
                    regions=regions,
                )
            ],
        )
        session_manager.send_payload(payload)

    def on_pre_close_window(self, window):
        if not session_manager.session_exists():
            return

        if not session_manager.session:
            return

        if not session_manager.enabled_for_window(window):
            return
        session_manager.delete_session()

    def _update_client_status(self, view):
        view.set_status(self.ACTIVE_CLIENTS, f'{len(session_manager.session.clients)}')


class LiveSessionViewEventListener(sublime_plugin.ViewEventListener):
    ACTIVE_CLIENTS = 'livesession.active_clients'

    def __init__(self, view: sublime.View) -> None:
        super().__init__(view)
        self._text_change_listener = (
            None
        )  # type: Optional[ref[LiveSessionTextChangeListener]]
        self.uid = ''
        self.relative_name = ''
        self.selection_set = []  # type: List[sublime.Region]
        self.last_selection = sublime.Region(-1, -1)
        self.workspace_ref = None
        self._manager = None  # type: Optional[SessionManager]

    def __del__(self) -> None:
        self.uid = None
        self.relative_name = None
        self.selection_set = []
        self.last_selection = sublime.Region(-1, -1)
        self.view.erase_status(self.ACTIVE_CLIENTS)
        self._deregister_view_async()

    @property
    def manager(self) -> SessionManager:
        if not self._manager:
            self._manager = session_manager

        assert self._manager
        return self._manager

    def _register_async(self) -> bool:
        self.workspace_ref = session_manager.add_view_to_session(
            self.view, self.view.settings().get('livesession.view.uid')
        )
        if not self.workspace_ref:
            # debug('not adding view to session')
            return False

        self._file_name = self.workspace_ref['relative_name']
        self.uid = self.view.settings().get('livesession.view.uid')

        buf = self.view.buffer()
        if not buf:
            debug(f'not tracking bufferless view {self.view.id()}')
            return False

        text_change_listener = LiveSessionTextChangeListener.ids_to_listeners.get(
            buf.buffer_id
        )
        if not text_change_listener:
            debug(f'couldn\'t find a text change listener for listener {self}')
            return False

        text_change_listener.view_listeners.add(self)
        self._text_change_listener = ref(text_change_listener)

        return True

    def _deregister_view_async(self) -> None:
        if self.manager.session_exists():
            self.manager.remove_view_from_session(self.uid)

    def on_post_move_async(self) -> None:
        if self.manager.session_exists() and self.manager.is_view_in_session(self.view):
            self._deregister_view_async()

    def on_load_async(self):
        '''
        Called when the file is finished loading.

        LiveSession: TYPE_FILE_SWITCHED | TYPE_FILE_OPENED
        '''
        if not self._register_async():
            return

        if (
            not self.manager.session_exists()
            or not self.manager.enabled_for_view(self.view)
            or not self.manager.enabled_for_window(self.view.window())
        ):
            return

        self._update_client_status()

        viewport = get_center_view_line_offset(self.view)
        iv, regions = encrypt_regions(
            [
                {
                    'a': 0,
                    'b': self.view.size(),
                    'content': self.view.substr(sublime.Region(0, self.view.size())),
                }
            ],
            self.manager.session.key,
            new_iv(),
            self.manager.session.encrypted,
        )

        syntax_info = get_syntax_info(self.view)
        payload = new_open_payload(
            uid=self.manager.session_id(),
            passphrase=self.manager.session_passphrase(),
            encrypted=self.manager.session.encrypted,
            host=self.manager.session_host(),
            sender=self.manager.me(),
            regions=[
                gen_region_info(
                    file_id=self.uid,
                    file_name=self.relative_name,
                    iv=iv,
                    syntax=syntax_info,
                    viewport={'a': viewport[0], 'b': viewport[1]},
                    regions=regions,
                )
            ],
        )

        self.manager.send_payload(payload)

    def on_activated_async(self):
        '''
        Called when the view gains input focus.
        Runs in a separate thread, and does not block the application.

        LiveSession: TYPE_FILE_OPENED
        '''

        if not self._register_async():
            return

        if (
            not self.manager.session_exists()
            or not self.manager.enabled_for_view(self.view)
            or not self.manager.enabled_for_window(self.view.window())
        ):
            return

        self._update_client_status()
        viewport = get_center_view_line_offset(self.view)
        iv, regions = encrypt_regions(
            [
                {
                    'a': 0,
                    'b': self.view.size(),
                    'content': self.view.substr(sublime.Region(0, self.view.size())),
                }
            ],
            self.manager.session.key,
            new_iv(),
            self.manager.session.encrypted,
        )

        syntax_info = get_syntax_info(self.view)
        payload = new_open_payload(
            uid=self.manager.session_id(),
            passphrase=self.manager.session_passphrase(),
            encrypted=self.manager.session.encrypted,
            host=self.manager.session_host(),
            sender=self.manager.me(),
            regions=[
                gen_region_info(
                    file_id=self.uid,
                    file_name=self.relative_name,
                    iv=iv,
                    syntax=syntax_info,
                    viewport={'a': viewport[0], 'b': viewport[1]},
                    regions=regions,
                )
            ],
        )

        self.manager.send_payload(payload)

    def on_selection_modified_async(self):
        '''
        LiveSession: Should be used to track where the users cursor is
        and send updates to the clients via TYPE_SELECTION_MODIFIED [Not Implemented]
        '''

        if not self._register_async():
            return

        if (
            not self.manager.session_exists()
            or not self.manager.enabled_for_view(self.view)
            or not self.manager.enabled_for_window(self.view.window())
        ):
            return

        regions = [{'a': region.a, 'b': region.b,} for region in self.view.sel()]
        self._update_selections()
        viewport = self.last_selection
        syntax_info = get_syntax_info(self.view)
        payload = new_selection_payload(
            uid=self.manager.session_id(),
            passphrase=self.manager.session_passphrase(),
            host=self.manager.session_host(),
            sender=self.manager.me(),
            regions=[
                gen_region_info(
                    file_id=self.uid,
                    file_name=self.relative_name,
                    syntax=syntax_info,
                    viewport={'a': viewport.a, 'b': viewport.b},
                    regions=regions,
                )
            ],
        )
        self.manager.send_payload(payload)

    def on_text_changed_async(self, changes: Iterable[sublime.TextChange]) -> None:
        if not self._register_async():
            return

        if (
            not self.manager.session_exists()
            or not self.manager.enabled_for_view(self.view)
            or not self.manager.enabled_for_window(self.view.window())
        ):
            return

        viewport = self.view.visible_region()
        iv, regions = encrypt_regions(
            [
                {'a': change.a.pt, 'b': change.b.pt, 'content': change.str}
                for change in changes
            ],
            self.manager.session.key,
            new_iv(),
            self.manager.session.encrypted,
        )
        syntax_info = get_syntax_info(self.view)
        payload = new_incremental_payload(
            uid=self.manager.session_id(),
            passphrase=self.manager.session_passphrase(),
            encrypted=self.manager.session.encrypted,
            host=self.manager.session_host(),
            sender=self.manager.me(),
            regions=[
                gen_region_info(
                    file_id=self.uid,
                    file_name=self.relative_name,
                    iv=iv,
                    syntax=syntax_info,
                    viewport={'a': viewport.a, 'b': viewport.b},
                    regions=regions,
                )
            ],
        )
        self.manager.send_payload(payload)

    def on_pre_close(self):
        if not self._register_async():
            return

        if (
            not self.manager.session_exists()
            or not self.manager.enabled_for_view(self.view)
            or not self.manager.enabled_for_window(self.view.window())
        ):
            return

        payload = new_closed_payload(
            uid=self.manager.session_id(),
            passphrase=self.manager.session_passphrase(),
            host=self.manager.session_host(),
            sender=self.manager.me(),
            regions=[gen_region_info(file_id=self.uid, file_name=self.relative_name,)],
        )
        self.manager.send_payload(payload)

    def _update_client_status(self):
        self.view.set_status(
            self.ACTIVE_CLIENTS, f'{len(self.manager.session.clients)}'
        )

    def _update_selections(self) -> None:
        pre_update = self.selection_set
        for sel in pre_update:
            if sel not in self.view.sel():
                self.selection_set.pop(self.selection_set.index(sel))

        for sel in self.view.sel():
            if sel not in self.selection_set:
                self.last_selection = sel
                self.selection_set.append(sel)


class LiveSessionTextCommand(sublime_plugin.TextCommand):
    def __init__(self, view: sublime.View) -> None:
        super().__init__(view)
        self._manager = None  # type: Optional[SessionManager]

    @property
    def manager(self) -> SessionManager:
        if not self._manager:
            self._manager = session_manager

        assert self._manager
        return self._manager

    def is_enabled(self):
        return self.manager.session_exists() and self.manager.enabled_for_window(
            self.view.window()
        )

    def is_visible(self):
        return self.manager.session_exists() and self.manager.enabled_for_window(
            self.view.window()
        )
