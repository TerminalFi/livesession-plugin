import os
import uuid

import sublime


def to_root_dir(path: str) -> str:
    path = to_filename(path)
    if '.' in path:
        separator_index = path.index('.')
        base_name = path[:separator_index]
        return base_name
    return path


def to_filename(path: str) -> str:
    return os.path.basename(path)


def normalize_path(path: str) -> str:
    if path and path[0] == '/':
        return path

    path = path.replace('\\', '/')
    if '/' not in path:
        path = f'/{path}'

    while path[0] != '/':
        path = path[1:]

    return path


def new_uuid() -> str:
    return str(uuid.uuid4())


def get_uid(view: sublime.View) -> str:
    return view.settings().get('livesession.view.uid', None)


def is_transient_view(view: sublime.View) -> bool:
    window = view.window()
    if window:
        if window.get_view_index(view)[1] == -1:
            return True  # Quick panel transient views
        return view == window.transient_view_in_group(window.active_group())
    else:
        return True


def update_active_client_status(view: sublime.View, count: int) -> None:
    view.set_status('livesession.active_clients', f'Active: {count}')


# REALLY bad hacks to try and scroll with host
def get_center_view_line_offset(view: sublime.View) -> (int, int):
    visible_region_lines = view.split_by_newlines(view.visible_region())
    center = int(len(visible_region_lines) / 2) - 1
    region = visible_region_lines[center]
    return (region.a, region.b)
